import { NgModule } from "@angular/core";
import { Routes , RouterModule } from "@angular/router";
// import { ContactComponent } from "src/app/contact/contact.component";
import { ContactdetailComponent } from "src/app/contactdetail/contactdetail.component";
import { NotfoundComponent } from "src/app/notfound/notfound.component";
import { CommonModule } from "@angular/common";
import { ContactModule } from "src/app/contact/contact.module";




const routeConfig: Routes = [
    // { path: 'contact', component: ContactComponent },
    { path: 'detail/:id', component: ContactdetailComponent },
    { path: '', redirectTo: '/contact', pathMatch: 'full' },//default contact
    { path: '**', component: NotfoundComponent }
];

@NgModule ({
    declarations: [
        // ContactComponent,
        ContactdetailComponent,
        NotfoundComponent,
        
    ],
    imports: [
        RouterModule.forRoot(routeConfig),
        CommonModule,
        ContactModule,
    ],
    exports: [RouterModule], //remember
    providers: [],
    bootstrap: [],
   
})

export class AppRoutingModuel{}
