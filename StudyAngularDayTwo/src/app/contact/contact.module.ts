import { NgModule } from "@angular/core";
import { ContactComponent } from "src/app/contact/contact.component";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

const routeConfig: Routes = [
    { path: 'contact', component: ContactComponent },

]

@NgModule({
    declarations: [ContactComponent],
    imports: [CommonModule,RouterModule.forChild(routeConfig)]

})

export class ContactModule{}