import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-study-ng-form',
  templateUrl: './study-ng-form.component.html',
  styleUrls: ['./study-ng-form.component.css']
})
export class StudyNgFormComponent implements OnInit {


  constructor() { }

  ngOnInit() {

  }
  onSubmit(loginForm) {
    console.log(loginForm.value)
  }

}
