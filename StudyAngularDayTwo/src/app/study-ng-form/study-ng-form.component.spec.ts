import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyNgFormComponent } from './study-ng-form.component';

describe('StudyNgFormComponent', () => {
  let component: StudyNgFormComponent;
  let fixture: ComponentFixture<StudyNgFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyNgFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyNgFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
