import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IpComponent } from './ip/ip.component';
import { HttpModule } from "@angular/http";
import { IpService } from 'src/app/ip/ip.service';
import { LoginComponent } from './login/login.component';
import { FormsModule } from "@angular/forms";
import { StudyNgFormComponent } from './study-ng-form/study-ng-form.component';
import { GetAPIComponent } from './get-api/get-api.component';
import { getAPI } from "./get-api/get-api.service";;
import { Router,RouterModule } from "@angular/router";
import { Routes } from '@angular/router/src/config';
import { AppRoutingModuel } from 'src/app/app-routing.module';
//import if you create file routing other 


// const routeConfig : Routes = [
//   {path :  'contact' , component : ContactComponent},
//   {path : 'detail',component: ContactdetailComponent},
//   {path : '',redirectTo: '/contact',pathMatch:'full'},//default contact
//   {path:'**',component: NotfoundComponent} 
// ];


@NgModule({
  declarations: [
    AppComponent,
    IpComponent,
   
    LoginComponent,
   
    StudyNgFormComponent,
   
    GetAPIComponent,
   

    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    // RouterModule.forRoot(routeConfig)
    AppRoutingModuel
  ],
  providers: [IpService,getAPI],
  bootstrap: [AppComponent]
})
export class AppModule { }
