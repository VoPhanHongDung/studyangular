import { Injectable } from "@angular/core";
import { Http } from "@angular/http";


@Injectable()

export class IpService{
    constructor(private http : Http){

    }

    getIP(){
        return this.http.get('https://jsonplaceholder.typicode.com/todos/1')
        .toPromise()
        .then(res => res.json())
        .then(resJson => resJson.title);
    }
}