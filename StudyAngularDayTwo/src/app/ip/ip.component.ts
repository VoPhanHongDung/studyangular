import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http";
import { IpService } from 'src/app/ip/ip.service';


@Component({
  selector: 'app-ip',
  templateUrl: './ip.component.html',
  styleUrls: ['./ip.component.css'],
  // providers: [IpService]
})
export class IpComponent implements OnInit {
  title : string ='';
  constructor(private ipService : IpService) {
   
  }

  ngOnInit() {
    this.ipService.getIP()
      .then(title => this.title = title)
      .catch(err => console.log(err));
  }
 
 

}
