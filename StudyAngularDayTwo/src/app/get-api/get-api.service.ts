import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ip } from "../Responsitory/ip";


@Injectable()

export class getAPI {
    ip : ip;
    constructor(private http: Http) {

    }

    getIP() {
        return this.http.get('http://ip.jsontest.com/')
            .toPromise()
            .then(res => res.json());
    }
}