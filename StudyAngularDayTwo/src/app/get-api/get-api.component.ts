import { Component, OnInit } from '@angular/core';
import { getAPI } from 'src/app/get-api/get-api.service';
import { ip } from 'src/app/Responsitory/ip';


@Component({
  selector: 'app-get-api',
  templateUrl: './get-api.component.html',
  styleUrls: ['./get-api.component.css']
})
export class GetAPIComponent implements OnInit {

  ip : ip;
  constructor(private getapiservice : getAPI) {
    this.getapiservice.getIP().then(resjson => {
      this.ip = resjson
      console.log(this.ip.ip)
    });
  }

  ngOnInit() {
    

  }

}
