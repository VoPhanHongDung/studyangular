import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , ParamMap } from '@angular/router';


@Component({
  selector: 'app-contactdetail',
  templateUrl: './contactdetail.component.html',
  styleUrls: ['./contactdetail.component.css']
})
export class ContactdetailComponent implements OnInit {

  constructor(private route : ActivatedRoute) {
    this.route.paramMap.subscribe((params : ParamMap)=>console.log(params.get('id')));
   }

  ngOnInit() {
  }

}
