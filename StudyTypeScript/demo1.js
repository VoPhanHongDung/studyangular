// var a : number = 100;
// var str : string = "hello";
// var isFalse : boolean = false;
// var d = {ho: "Vo" , ten : "Dung"};
// console.log(a);
// console.log(str);
// console.log(isFalse);
// console.log(d);
// var c : boolean = false ;
// if(c == false){
//     console.log("false");
// }else {
//     console.log("true");
// }
// for (var i = 0 ; i< 5;i++){
//     console.log("hello");
// }
// var arrayList : string[] = ["dzung","joey"];
// console.log(arrayList[1]);
// console.log(arrayList.length); //count number obj 
// //duet mang : js 
// for(var i = 0 ; i < arrayList.length ; i++)
// {
//     console.log(arrayList[i]);
// }
// //duyet mang : for in/off
// for (var j in arrayList ){
//     console.log("IN" + j);
// }
// for (var k of arrayList) {
//     console.log("of" + k);
// }
// function printSTR(){
//     console.log("hello have a good day !");
// }
// printSTR();
// function getNumber(){
//     return "get any number";
// }
// var str : string = getNumber();
// console.log(str);
var Student = /** @class */ (function () {
    function Student(id, name, dob) {
        this.id = id;
        this.name = name;
        this.DOB = dob;
    }
    Student.prototype.setClassNumber = function (classNumber) {
        if (classNumber < 0) {
            this.classNumber = 1;
        }
        else {
            this.classNumber = classNumber;
        }
    };
    Student.prototype.getClassNumber = function () {
        return this.classNumber;
    };
    return Student;
}());
var joey = new Student("1", "Joey", 20);
var recheal = new Student("2", "Recheal", 20);
joey.setClassNumber(10);
recheal.setClassNumber(-1);
console.log(joey.name);
console.log(joey.getClassNumber());
console.log(recheal.name);
console.log(recheal.getClassNumber());
